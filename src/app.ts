/**
 * @file    app.js
 * @desc    Main code section of the microservice.
 *          Here the express is initialized, as well the Swagger and the Swagger UI.
 * @since   Februray, 2020
 */
'use strict';


// Requires
var SwaggerExpress = require('swagger-express-mw');
var SwaggerTools = require('swagger-tools');
var app = require('express')();
var YAML = require('yamljs');
var swaggerDoc = YAML.load('./src/api/swagger/swagger.yaml');
var orm = require('orm');

// Database connection details
const DB_TYPE = "mysql";
const serverUrl = "http://127.0.0.1";

//let ORM_CONNECT_URL = `${DB_TYPE}://${USERNAME}:${PASSWORD}@${HOST}/${DATABASE_NAME}`;


// Injection of the ORM to the requests
// app.use(orm.express(ORM_CONNECT_URL, {
//     define: function (db, models, next) {
//         // Require models
//         models.user = require('./api/models/UserModel')(db, {});
//         models.battalion = require('./api/models/BattalionModel')(db, {});
//         models.company = require('./api/models/CompanyModel')(db, {});
//         models.squad = require('./api/models/SquadModel')(db, {});
//         models.corps = require('./api/models/CorpsModel')(db, {});


//         // Models relations
//         models.company.hasOne('battalion', models.battalion, {reverse: 'companies', required: true});
//         models.squad.hasOne('company', models.battalion, {reverse: 'squads', required: true});
//         models.user.hasOne('battalion', models.battalion, {required: true}, {autoFetch: true, autoFetchLimit: 1});
//         models.user.hasOne('company', models.company, {required: true}, {autoFetch: true, autoFetchLimit: 1});
//         models.user.hasOne('squad', models.squad, {required: true}, {autoFetch: true, autoFetchLimit: 1});
//         models.user.hasOne('corps', models.corps, {required: true}, {autoFetch: true, autoFetchLimit: 1});



//         // Syncing db
//         db.sync(function(err) {
//             console.log("Notice: Init & Syncing Database ORM models.");
//         });
//         next();
//     }

// }));

// Initialize of the swaggerUI
SwaggerTools.initializeMiddleware(swaggerDoc, function(middleware: any) {
    app.use(middleware.swaggerUi());
  });

// Export setup Promise for testing
module.exports = new Promise(function (resolve, reject) {

  var config = {
    appRoot: __dirname 
  };

  SwaggerExpress.create(config, function (err: any, swaggerExpress: any) {
    if (err) { throw err; }

    // Install middleware
    swaggerExpress.register(app);

    var port = process.env.PORT || 10010;
    app.listen(port, function() {
        console.log('\x1b[32m%s\x1b[0m', '');  
        console.log('[system] Microservice start running...');  
        console.log('\x1b[32m%s\x1b[0m', '');  
        console.log('\x1b[36m%s\x1b[0m', '----------------------------------------------------------------------------');
        console.log('\x1b[36m%s\x1b[0m', `|   Bhd01Online API Documentation runs at: ${serverUrl}:${port}/docs    |`);
        console.log('\x1b[36m%s\x1b[0m', '----------------------------------------------------------------------------\n');
      resolve(app);
    });
  });
});